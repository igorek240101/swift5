// Создать класс родитель и 2 класса наследника
class Figure // Родитель
{
    func S() -> Double // Абстрактный метод
    {   
        fatalError("Implement this method")
    }
}

class Triangle : Figure // Тругоольник - частый случай фигуры
{
    let a, b, c : Double;
    override func S() -> Double // Его площадь мы уже можем посчитать
    {   
        let p = (a + b + c) / 2; // Полупериметр
        return (p * (p - a) * (p - b) * (p - c)).squareRoot() // Формула Герона
    }
    
    init(a : Double, b : Double, c : Double) // Тругольник задаем тремя сторонами
    {
        self.a = a;
        self.b = b;
        self.c = c;
    }
}

class Rectangular : Figure // Прямоугольник - частный случай фигуры
{
    let a, b : Double;
    
    override func S() -> Double // Его площадь мы уже можем посчитать
    {   
        return a * b;
    }
    
    init(a : Double, b : Double)
    {
        self.a = a;
        self.b = b;
    }
}

// Создадим массив фигур и произведем "упаковку" производных типов
let figures : [Figure] = [Triangle(a : 5, b : 4, c : 3), Rectangular(a : 2, b : 3), Rectangular(a : 4, b : 4)];
// Протестируем вызов расчета площади
for i in figures
{
    print(i.S());
}
// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House
{
    let width, height : Double;
    var isActive : Bool = true; // Существует ли дом
    let id : Int; // Уникальный идентификатор
    static var count : Int = 0; // Количество созданных домов
    func create()
    {
        isActive = true // Если дом  был уничтожен, построим его заново
        print("Создан дом id = \(id), площадью в \(width * height) квадратных метра");
    }
    func destroy()
    {
        if isActive // Только  если дом существует его можно уничтожить
        {
            isActive = false;
            print("Дом \(id) уничтожен");
        }
    }
    init(width : Double, height : Double)
    {
        self.width = width;
        self.height = height;
        id = House.count + 1;
        House.count += 1;
    }
    deinit // Если дом удаляется из памяти его надо уничтожить
    {
        destroy();
    }
}

// Создадим несколько домов для проверки
var house : House = House(width : 5, height : 20);
house.create();
house.destroy();
house = House(width : 10, height : 5);
house.create(); // Здесь Дом2 😜 все равно будет уничтожен, т.к. на него ссылок больше нет, он будет удален из памяти, предварительно вызвав destroy в deinit
house = House(width : 20, height : 4);
house.create();
house.destroy();

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class Students
{
    var student : [(name : String, age : Int, class_ : Class, avgPoint : Double)]; // Массив учеников
    
    init(student : [(name : String, age : Int, class_ : Class, avgPoint : Double)])
    {
        self.student = student;
    }
    
    enum Class // Класс - буква и число
    {
        case A (number : Int)
        case B (number : Int)
        case C (number : Int)
        
        func toString() -> String
        {
            switch self
            {
                case .A(let number) : return "\(number)A";
                case .B(let number) : return "\(number)Б";
                case .C(let number) : return "\(number)В";
            }
        }
    }
    
    func printStudent() // Вывод массива учеников
    {
        for i in student
        {
            print("Имя \(i.name)\r\nВозраст \(i.age)\r\nКласс \(i.class_.toString())\r\nСредний бал \(i.avgPoint)");
        }
    }
    
    func sortName() // Сортировка по имени
    {
        student = student.sorted{$0.name < $1.name}
    }
    func sortAge() // Сортировка по возрасту
    {
        student = student.sorted{$0.age < $1.age}
    }
    func sortClass() // По классу (число)
    {
        student = student.sorted
        {
            var first = $0.class_.toString();
            var second = $1.class_.toString();
            first.removeLast(1);
            second.removeLast(1);
            return Int(first)! < Int(second)!;
        }
    }
    func sortAvgPoint() // Среднему балу
    {
        student = student.sorted{$0.avgPoint < $1.avgPoint}
    }
}

// Создадим массив и протестируем
let students : Students =  Students(student : [(name : "Петров Петр Петрович", age : 13, class_ : Students.Class.A(number : 7), avgPoint : 4.7),
                                                (name : "Васильев Василий Васильевич", age : 17, class_ : Students.Class.B(number : 11), avgPoint : 3.2),
                                                (name : "Александров Александр Александрович", age: 7, class_ : Students.Class.C(number : 7), avgPoint : 5.0)]); //Последний вундеркинд просто
students.printStudent();
students.sortName();
students.printStudent();
students.sortAge();
students.printStudent();
students.sortClass();
students.printStudent();
students.sortAvgPoint();
students.printStudent();

// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов
struct Card : Hashable // Это структура
{
    let side : Side;
    let value : Int; // Номинал карты
    
    enum Side : Int // Масть карты импользуем rawValue для удобного рандома
    {
        case Spades = 1
        case Clubs = 2
        case Diamonds = 3
        case Hearts = 4
    }
    
    init(side : Side, value : Int)
    {
        self.side = side;
        self.value = value;
    }
    
    var stringValue : String // Привидем номинал к строковому зачению
    {
        if value > 10
        {
            switch value
                {
                    case 11: return "Валет";
                    case 12: return "Дамма";
                    case 13: return "Король";
                    default: return "Туз";
                }
        }
        else
        {
                return "\(value)";
        }
    }
    
    var stringSide : String // Приведем масть к строковому значению
    {
        switch side
        {
                case .Spades : return "пики";
                case .Clubs : return "трефы";
                case .Diamonds : return "\u{001B}[0;31mбуби\u{001B}[0;0m";
                case .Hearts : return "\u{001B}[0;31mчерви\u{001B}[0;0m";
        }
    }
    
    static func comp(card1 : Card, card2 : Card) -> Bool { card2.value > card1.value; } // компаратор для сортировки (по номиналу)
}

class Flat : House // А это класс (см 168)
{
    let flor : Int;
    override func create()
    {
        isActive = true
        print("Создана квартира id = \(id), площадью в \(width * height) квадратных метра на \(flor) этаже"); // Здесь орпделен немного другой текст
    }
    init(width : Double, height : Double, flor : Int)
    {
        self.flor = flor; // А ещё дополнительное поле есть
        super.init(width : width, height : height);
    }
}

/*
    Главное отличие класса и структуры - это то, что экзмепляры класса - ReferenceType, а экзмепляры структуры - ValueType
    То есть в переменной типа структуры будет находиться сам экземпляр, а в переменной типа класса будет находиться ссылка на ээкзмепляр
    => получить доступ к (конкретному) экзмепляру структуры можно только через переменную, где он непосредствено храниться
    а к экзмепляру класса можно получить доступ из нескольких мест т.к. значение ссылки можно записать в несколько переменных
    Так же классы в отличии от структур обладют способность к наследованию. Поэтому классы предпочтительнее при реализации ООП-подхода
    => например прием с упаковкой типов привденный в самом начале программы не может быть реализован на структурах
    В заключении можно сделать вывод что сртуткуры удобны при описании конечных типов, которые используются внутри классов,
    описывающих макро-логику
*/

// Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт
let poker : Poker = Poker(); // Создадим стол
poker.Game(); // Начнем игру

class Poker
{
    var cards : [Card]; // Карты в руке
    let combination = [flashRoyal, streetFlash, care, fullHouse, flash, street, third, twoPair, pair, maxCard]; // функции проверки комбинаций
    var prevCombination : [(text : String, cards : [Card])] = [] // История рук
    func StardedGame() // Раздача карт
    {
        var setCards : Set<Card> = []; // В колоде не может быть одинаковых карт поэтому используем set
        while setCards.count < 5 // Подход немного в лоб, но статестически шагов в любом случае будет немного
        {
            setCards.insert(Card(side : Card.Side(rawValue : Int.random(in : 1...4))!, value : Int.random(in : 2...14)));
        }
        cards = Array(setCards);
    }
    
    func Game()
    {
        print("Вы садитесь за стол");
        print("Чтобы разрадть карты введите \"1\"");
        print("Чтобы посмотреть прошлые раздачи введите \"2\"");
        print("Чтобы выйти из-за стола введите \"3\"");
        while true
        {
            print("Вы решаете: ");
            let comand : String = readLine()!;
            switch comand
            {
                case "1" : do
                {
                    print("начать игру");
                    StardedGame(); // Раздаем карты
                    let sort : [Card] = cards.sorted(by : Card.comp); // Отсортируем руку по возрастанию номинала
                    for i in combination // Проверка выпавшей комбинации (от флеш-рояля до старшей карты) 
                    {
                        var text : String = "";
                        /* Важные условия корректной работы проверки комбинаций
                            1) Карты отсоритрованы по возрастанию номинала (реализовано в 281)
                            2) Проверки выполняются от старшей комбинации к младшей (реализовано в 253)
                            3) Всегда ровно 5 карт в руке (реализовано в 258)
                        */
                        if i(sort, &text) // true, если комбинация совпала
                        {
                            print(text); // Выведем комбинацию
                            for j in cards // А затем руку
                            {
                                print("\(j.stringValue) \(j.stringSide)");
                            }
                            prevCombination.append((text : text, cards : cards)); // Добавим руку в историю
                            break; // Дальше комбинации проверять не нужно, выходим
                        }
                    }
                }
                case "2" : do
                {
                    print("проанализировать прошлые раздачи");
                    for i in prevCombination // Просто выводим историю рук
                    {
                        print(i.text);
                        for j in i.cards
                        {
                            print("\(j.stringValue) \(j.stringSide)");
                        }
                    }
                }
                case "3" : do
                {
                    print("встать из-за стола");
                    return; // А здесь просто завершение игры
                }
                default : do {}
            }
        }
    }
    
    init()
    {
        cards = [];
    }
    
    static func flashRoyal(cards : [Card], text : inout String) -> Bool
    {
        let side : Card.Side = cards[0].side;
        for i in 0...4
        {
            if cards[i].side != side || cards[i].value != 10 + i // Если карты не одной масти или их номинал не от 10 до 14
            {
                return false; // тогда это не флеш-рояль
            }
        }
        text = "У вас \(cards[0].stringSide) флеш-рояль"
        return true;
    }
    
    static func streetFlash(cards : [Card], text : inout String) -> Bool
    {
        let side : Card.Side = cards[0].side;
        for i in 1...4
        {
            if cards[i].side != side || cards[i].value != cards[i - 1].value + 1 // Если карты не одной масти или не идут подряд
            {
                return false; // тогда это не Стрит-флеш
            }
        }
        text = "У вас \(cards[0].stringSide) Стрит-флеш, старшая карта - \(cards[4].stringValue)"
        return true;
    }
    
    static func care(cards : [Card], text : inout String) -> Bool
    {
        // Если первая и четвертая карты равны по номиналу, то и между ними карты равны (т.к. массив осортирован)
        // Ещё надо проверить вторую и последнюю, если равны, то и между ними карты равны по номиналу
        // Условие написано от обратного
        if cards[0].value != cards[3].value && cards[4].value != cards[1].value
        {
            return false;
        }
        text = "У вас Каре из \(cards[2].stringValue)"
        return true;
    }
    
    static func fullHouse(cards : [Card], text : inout String) -> Bool
    {
        if (cards[0].value == cards[2].value && cards[3].value == cards[4].value) || // Тройка и пара
            (cards[4].value == cards[2].value && cards[0].value == cards[1].value) // или пара и тройка
        {
            if cards[0].value == cards[2].value // но выводим сначала тройку
            {
                text = "У вас фулл-хаус из \(cards[0].stringValue) и \(cards[4].stringValue)"
            }
            else
            {
                text = "У вас фулл-хаус из \(cards[4].stringValue) и \(cards[0].stringValue)"
            }
            return true;
        }
        return false;
    }
    
    static func flash(cards : [Card], text : inout String) -> Bool
    {
        let side : Card.Side = cards[0].side;
        for i in 1...4
        {
            if cards[i].side != side // Если все карты не одной масти
            {
                return false; // тогда это не флэш
            }
        }
        text = "У вас \(cards[0].stringSide) Флеш, старшая карта - \(cards[4].stringValue)"
        return true;
    }
    
    static func street(cards : [Card], text : inout String) -> Bool
    {
        for i in 1...4
        {
            if cards[i].value != cards[i - 1].value + 1 // Если карты не идут друг за другом
            {
                return false; // тогда это не стрит
            }
        }
        text = "У вас Стрит, старшая карта - \(cards[4].stringValue)"
        return true;
    }
    
    static func third(cards : [Card], text : inout String) -> Bool
    {
        // Если это тройка, то 3-я карта всегда будет в ней, надо лишь проверить 3 случая когда 3-я карта используется в 3-ке
        if (cards[0].value == cards[2].value) || (cards[4].value == cards[2].value) || (cards[1].value == cards[3].value)
        {
            text = "У вас тройка - \(cards[2].stringValue)";
            return true;
        }
        return false;
    }
    
    static func twoPair(cards : [Card], text : inout String) -> Bool
    {
        var count : Int = -1
        for i in 1...4
        {
            if cards[i].value == cards[i - 1].value // Две карты с одинаковым номиналом
            {
                if count == -1 // Если такого ещё не было запмним текущую позицию 
                {
                    count = i;
                }
                else
                {
                    text = "У вас две пары - \(cards[i].stringValue) и \(cards[count].stringValue)"; //Значение карты в i всегда больше, чем в count
                    return true;
                }
            }
        }
        return false;
    }
    
    static func pair(cards : [Card], text : inout String) -> Bool
    {
        for i in 1...4
        {
            if cards[i].value == cards[i - 1].value // Две карты с одинаковым номиналом
            {
                text = "У вас пара - \(cards[i].stringValue)";
                return true;
            }
        }
        return false;
    }
    
    static func maxCard(cards : [Card], text : inout String) -> Bool
    {
        text = "У вас старшая карта \(cards[4].stringValue) \(cards[4].stringSide)"; // Просто берем последню карту, она будет самая большая
        return true;
    }
}